library(tidyverse)

genome_transcriptome <-
  tibble(split=0:49) %>% 
  mutate(train=map(split,~read_csv(file=paste0("/home/dylan/Documents/causal_multi_omics/data/residuals_findingpheno/train/cv",.,".csv"),show_col_types = FALSE))) %>% 
  mutate(test=map(split,~read_csv(file=paste0("/home/dylan/Documents/causal_multi_omics/data/residuals_findingpheno/validation/cv",.,".csv"),show_col_types = FALSE)))



load(file="/home/dylan/Documents/causal_multi_omics/data/train-test.Rdata")
transcriptome <- bind_rows(trainData,testData) %>% 
  mutate(Sample.ID=as.character(fish)) %>% 
  select(Sample.ID,weight,gene,expr) %>% 
  pivot_wider(names_from = gene, values_from = expr)
rm(trainData,testData)
transcriptome

N_expressions <- ncol(transcriptome)-2
# remark: summary(lm()) is faster than drop1(lm())
system.time(
  genome_transcriptome <- genome_transcriptome %>% 
    mutate(pval=map(train,function(df){
      df <- left_join(df,transcriptome,by = join_by(Sample.ID))
      pval <- tibble(
        gene=colnames(transcriptome)[-(1:2)],
        pval_R=sapply(1:N_expressions,function(i){
          summary(lm(df[[2]]~df[[3+i]]))$coefficients[2,"Pr(>|t|)"]}),
        pval_Y=sapply(1:N_expressions,function(i){
          summary(lm(df[[3]]~df[[3+i]]))$coefficients[2,"Pr(>|t|)"]}),
      )
      # find genes and return results
      mutate(pval,select=(pval_Y < 0.05/sum(pval$pval_R >= pval_R)))
    })) %>% 
    mutate(number_genes=map_dbl(pval,~sum(.$select)),
           max_pval_R=map_dbl(pval,~max(.$pval_R[.$select])))
)


genome_transcriptome %>% select(split,number_genes,max_pval_R) %>% print(n=Inf)


